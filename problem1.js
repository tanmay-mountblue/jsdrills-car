
module.exports.problem1 = (inventory,id) =>{
    let flag=0;
    if(id===null || id=== undefined || id==='' || !inventory.length){
        return error={msg:"Invalid Inputs"}
    }
    for (let index = 0; index < inventory.length; index++) {
        if(inventory[index].id===id){
            // return `Car ${id} is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`;
            flag=1;
            return inventory[index];
        }        
    }
    if(!flag)
        return error={msg:"ID not found!!!"};
    
    return error={msg:"Something went Wrong!!!"};    
}
module.exports.problem3 = (inventory) =>{
    for(let i=0;i<inventory.length;i++){
        for(let j=0;j<inventory.length-i-1;j++){
            if(inventory[j].car_model.localeCompare(inventory[j+1].car_model)===1){
                let temp=inventory[j];
                inventory[j]=inventory[j+1];
                inventory[j+1]=temp;
            }
        }
    }
    return inventory;
}
const { problem1 } = require('../problem1.js');
const inventory = require('./inventory.js');
let id=33;
const result = problem1(inventory,id);
if(result.msg){
    console.log(result.msg);
}else{
    console.log(`Car ${id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}
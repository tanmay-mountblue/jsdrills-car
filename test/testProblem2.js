const { problem2 } = require('../problem2.js');
const inventory = require('./inventory.js');
const result = problem2(inventory);
console.log(`Last car is a ${inventory[result].car_make} ${inventory[result].car_model}`)